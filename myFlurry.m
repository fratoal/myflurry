//
//  myFlurry.h
//
//  Created by Francesc Tovar on 13/04/12.
//  Copyright (c) 2012 Play Creatividad. All rights reserved.
//  Flurry SDK Required: http://www.flurry.com/

#import "myFlurry.h"

@implementation myFlurry

+ (void) startFlurry {
    NSSetUncaughtExceptionHandler(&uncaughtExceptionHandler);
    [FlurryAnalytics startSession:FLURRY_ID];
    NSLog(@"Flurry Started");
}

+ (void) flurryEventWithName: (NSString *) name {
    [FlurryAnalytics logEvent:name];
    NSLog(@"An event ocurred: %@", name);
}

+ (void) flurryErrorWithName: (NSString *) n Message: (NSString *) m andException: (id) ex {
    [FlurryAnalytics logError:n message:m exception:ex];
}

void uncaughtExceptionHandler(NSException *exception) {
    [FlurryAnalytics logError:@"Uncaught" message:@"Crash!" exception:exception];
}

@end
