//
//  myFlurry.h
//
//  Created by Francesc Tovar on 13/04/12.
//  Copyright (c) 2012 Play Creatividad. All rights reserved.
//  Flurry SDK Required: http://www.flurry.com/

#import <UIKit/UIKit.h>
#import "FlurryAnalytics.h"

//Definition of the unique App Key
#define FLURRY_ID           @"YOUR_FLURRY_KEY"

//Definition of various events
#define FLURRY_EVENT1       @"Hello World"
#define FLURRY_EVENT2       @"Hello World 2"

@interface myFlurry : NSObject

//Use this method in your App Delegate
+ (void) startFlurry;

//Add desired flurry event
+ (void) flurryEventWithName: (NSString *) name;

//Add flurry error messages
+ (void) flurryErrorWithName: (NSString *) n Message: (NSString *) m andException: (id) ex;

@end
